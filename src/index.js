import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";
let id = 0;
function Todo(props) {
  return (
    <li>
      <input
        type="checkbox"
        checked={props.todo.checked}
        onChange={() => props.onToggle()}
      />
      <button
        onClick={() => {
          props.onDelete();
        }}
      >
        Delete
      </button>
      <span> {props.todo.text}</span>
    </li>
  );
}

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      todoList: []
    };
  }
  addTodo() {
    const text = prompt("Text TODO");
    this.setState({
      todoList: [
        ...this.state.todoList,
        {
          id: id++,
          text: text,
          checked: false
        }
      ]
    });
  }
  removeTodo(id) {
    this.setState({
      todoList: this.state.todoList.filter(todo => todo.id !== id)
    });
  }
  toggleTodo(id) {
    this.setState({
      todoList: this.state.todoList.map(todo => {
        if (todo.id != id) return todo;
        return {
          id: todo.id,
          text: todo.text,
          checked: !todo.checked
        };
      })
    });
  }
  render() {
    return (
      <div className="App">
        <h1>TODO app</h1>
        <p>
          Todo count: {this.state.todoList.length} &nbsp;&nbsp;  
          Todo unchecked: {this.state.todoList.filter(todo => !todo.checked).length}
        </p>

        <button onClick={() => this.addTodo()}>New TODO</button>
        <ul>
          <li>
            {this.state.todoList.map(todo => (
              <Todo
                onToggle={() => this.toggleTodo(todo.id)}
                onDelete={() => this.removeTodo(todo.id)}
                todo={todo}
              />
            ))}
          </li>
        </ul>
      </div>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App name={"Emmanuel"} />, rootElement);
